<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Post extends Model
{
    protected $fillable = ['title', 'body', 'likes'];

    protected $attributes = [
        'likes' => 0
    ];
    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function showDate()
    {
        return Carbon::parse($this->created_at)->format('d/m/Y H:i');
    }
}
