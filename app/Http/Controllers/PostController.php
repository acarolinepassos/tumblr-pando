<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Post;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //$post = Post::orderBy('created_at', 'desc')->get();
       $post = DB::table('posts')->orderBy('created_at', 'desc')->paginate(2);

        return view('home', ['posts' => $post]); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = new Post;
        $post->id       = $request->id;
        $post->title    = $request->title;
        $post->likes    = 0;
        $post->body     = $request->body;
        $post->user_id  = \Auth::user()->id;

        $post->save();

        return redirect('/')->with('alert-success', 'Post salvo com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        $comments = $post->comments;

        
        return view('posts.show', ['post' => $post, 'comments'=> $comments]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        return view('posts.create', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::findOrFail($id);
        $post->id       = $request->id;
        $post->title    = $request->title;
        $post->likes    = $request->likes;
        $post->body     = $request->body;

        $post->save();

        return redirect('/')->with('alert-success', 'Post salvo com sucesso!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::findOrFail($id);
        $post->delete();
        return redirect('/')->with('alert-success', 'Post deletado com sucesso!');;
    }

    public function count_like($id){
        $post = Post::findOrFail($id);
        // dd($post);
        $post->increment('likes', 1);
        $post->save();
        return back();
    }


}
