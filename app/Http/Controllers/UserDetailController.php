<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\UploadTrait;
use App\UserDetail;
use App\User;
use Illuminate\Support\Facades\Auth;



class UserDetailController extends Controller
{
    use UploadTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $user = User::find($id);
        if(isset($user->detail))
        {
            return view('perfil', ['user' => $user]);
        }
        else
         {
            return redirect()->route('criarperfil',['user' => $user])->with('error', 'Usuario nao possui detalhes ainda!');

        }
            
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('userdetails.create', ['user' => \Auth::user()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $detail = new UserDetail;

        $detail->endereco   = $request->endereco;
        $detail->telefone   = $request->telefone;
        $detail->user_id   =  \Auth::user()->id;

        $file_path = 'photos/'; 
        if($request->file('photo'))
       { 
           $detail->photo = $this->uploadFile($request->file('photo'), $file_path); 
       } 
       else 
       {
       $detail->photo = 'http://www.personalbrandingblog.com/wp-content/uploads/2017/08/blank-profile-picture-973460_640-300x300.png';
    }
    $detail->save();
    $user = User::find($detail->user_id);

       return view('perfil', ['user' => $user])->with('alert-success','User has been stored!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $detail = UserDetail::findOrFail($id);
        return view('userdetails.show', ['userdetails' => $detail]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $detail = UserDetail::findOrFail($id);
        $user = User::find($detail->user_id);
        return view('userdetails.create', ['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $user->name   = $request->name;
        $user->email   = $request->email;
       // $user->   = $request->email;
        $user->detail->endereco   = $request->endereco;
        $user->detail->telefone   = $request->telefone;


        $file_path = 'photos/';
        if($request->file('photo'))
       { 
           $user->detail->photo = $this->uploadFile($request->file('photo'), $file_path); 
       }
       $user->save();
       $user->detail->save();

       Auth::setUser($user);

    //    $user = User::find(\Auth::user()->id);
        return view('perfil', ['user' => $user])->with('alert-success','User has been updated!');
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $detail = UserDetail::findOrFail($id);
        $detail->delete();
        return redirect('/')->with('alert-success','Product has been deleted!');
    }
}