<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;


class UserController extends Controller
{
    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('auth.register', ['user' => $user]);
    }

    protected function update(Request $request, $id){
        $user= User::findOrFail($id);
        if(User::where('email', $request->email)->get()->isEmpty())
        {
        $user->email   = $request->email;
        $user->name    = $request->name;
        $user->save();
        return redirect('/')->with('message', 'User updated successfully!');
        }       
        else
        {
        return redirect('/')->with('error', 'Email já cadastrado!');
        }
    }
}