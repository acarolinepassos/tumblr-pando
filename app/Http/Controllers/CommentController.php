<?php

namespace App\Http\Controllers;
use App\Comment;
use App\Post;

use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
     
    // public function index()
    // {
    //     $comment = Comment::all();
    //     return view('comments.show');
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function create()
    // {
    //     return view('.create');
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $comment = new Comment;
        $comment->id       = $request->id;
        $comment->body     = $request->body;
        $comment->post_id  = $request->post_id;
        $comment->user_id  = \Auth::user()->id;
        $comment->save();  
  

        return redirect('/')->with('alert-success', 'comment stored!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function edit($id)
    // {
    //     $comment = Comment::findOrFail($id);
    //     return view('comments.register', ['comment' => $comment]);
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request, $id)
    // {
    //     $comment = Comment::findOrFail($id);
    //     $comment->id       = $request->id;
    //     $comment->title    = $request->title;
    //     $comment->likes    = $request->likes;
    //     $comment->body     = $request->body;
    //     $comment->save();
    //     return redirect('/')->with('alert-success', 'comment updated');

        
    // }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comment = Comment::findOrFail($id);
        $comment->delete();
        return redirect('/')->with('alert-success','Product hasbeen deleted!');
    }
}
