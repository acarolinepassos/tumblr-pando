@extends('layouts.app') @section('content')
<br>
<br>
<div class="row">
  <div class="col-md-3"></div>
  <div class="col-md-6">
    <div class="box box-primary">

      <div class="box-body box-profile">
        <img class="profile-user-img img-responsive img-circle" src="{{url($detail->photo)}}" alt="User profile picture">

        <h3 class="profile-username text-center">{{$detail->}}</h3>


        <ul class="list-group list-group-unbordered">
          <li class="list-group-item">
            <b>Posts</b>
            <a class="pull-right">{{$post_quant}}</a>
          </li>
          <li class="list-group-item">
            <b>Email</b>
            <a class="pull-right">{{$user->email}}</a>
          </li>
        </ul>
      </div>
      <a class="btn btn-block btn-default" href="{{url('registrar/'.$user->id)}}">Editar</a>
      </a>
    </div>
  </div>
  <div class="col-md-3"></div>
</div>
@endsection

