@extends('layouts.app') @section('content')

<br>
<br>
<div class="row">
  <div class="col-md-3"></div>
  <div class="col-md-6">
    <div class="box box-primary">

      <div class="box-body box-profile">
        <div class="row">
          <div class="col-md-offset-5 col-md-2">
              <img class="img img-responsive image-circle" src="{{route('file', $user->detail->photo)}}" alt="User profile picture">
            </div>
        </div>
        <h3 class="profile-username text-center">{{$user->name}}</h3>


        <ul class="list-group list-group-unbordered">
          <li class="list-group-item">
            <b>Posts</b>
            @if(isset($user->posts))
            <a class="pull-right">{{$user->posts->count()}}</a>
            @else
            <a class="pull-right">0</a>
            @endif
          </li>
          <li class="list-group-item">
            <b>Email</b>
            <a class="pull-right">{{$user->email}}</a>
          </li>
          <li class="list-group-item">
            <b>Endereco</b>
            <a class="pull-right">{{$user->detail->endereco}}</a>
          </li>
          <li class="list-group-item">
            <b>telefone</b>
            <a class="pull-right">{{$user->detail->telefone}}</a>
          </li>
        </ul>
      </div>
      <a class="btn btn-block btn-default" href="{{url('editdetail/'.$user->detail->id)}}">Editar</a>
      </a>
    </div>
  </div>
  <div class="col-md-3"></div>
</div>
@endsection