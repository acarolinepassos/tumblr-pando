@extends('layouts/app') 

@section('content')

<section class="content-header">
    <h1>
        Adicionar Post
        <small>Preview</small>
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="#">
                <i class="fa fa-dashboard"></i> Inicio
            </a>
        </li>
        <li>
            <a href="#">Formulário</a>
        </li>
        <li class="active">Adicionar Post</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Post</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                @if(isset($post))
                <form role="form" action="{{url('/atualizar/'.$post->id)}}" method="POST">
                    @else
                    <form role="form" action="{{url('/post')}}" method="POST">
                        @endif {{csrf_field()}}
                        <div class="box-body">
                            <div class="form-group">
                                <label for="title">Título</label>
                                <input type="text" name="title" class="form-control" value="{{(isset($post) ? $post->title : null )}}" placeholder="Enter title">
                            </div>
                            <div class="form-group">
                                <label for="text">Texto</label>
                                    <textarea id="editor" class="form-control" name="body" rows="3" placeholder="Enter text...">{{(isset($post) ? $post->body : null )}}</textarea>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Enviar</button>
                            </div>
                    </form>
                    </div>
            </div>
        </div>
</section>


@endsection

@section('scripts')
<script>
    $( document ).ready(function() {
        $('#editor').ckeditor();
    });
</script>
@endsection