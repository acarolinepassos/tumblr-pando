@extends('layouts.app') @section('content')




<div class="row">
  <div class="col-md-3"></div>
  <div class="col-md-6">
    @if(session('message'))
    <div class="success alert-success">
      {{session('message')}}
    </div>
    @endif @if(session('error'))
    <div class="alert alert-danger">
      {{session('error')}}
    </div>
    @endif

    <h2 class="page-header">Posts</h2>
    <div class="box">

    </div>


  </div>
  <div class="col-md-3"></div>
</div>
<div class="row">
  <div class="col-md-6 col-md-offset-3">
    <div style="display:inline">
      <div class="box box-widget">
        <div class="box-header with-border">
          <div class="user-block">
            <img class="image" src="/{{$post->user->detail->photo}}" alt="User Image">
            <span class="username">{{$post->user->name}}</span>
            <span class="description">{{$post->showDate()}}</span>
          </div>
          <!-- /.user-block -->
          <div class="box-tools">
            <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="" data-original-title="Mark as read">
              <i class="fa fa-circle-o"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="collapse">
              <i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove">
              <i class="fa fa-times"></i>
            </button>
          </div>
          <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="box-header with-border">
            <h3 class="box-title">{{$post->title}}</h3>

            <a class="btn btn-app" href="{{url('editpost/'.$post->id)}}">
              <i class="fa fa-edit"></i>Editar</a>
          </div>
          <div id="box-body" class="box-body">
            <div class="row">
              <div class="col-md-12">

                <!-- post text -->
                {!!html_entity_decode($post->body)!!}
              </div>
            </div>

            <!-- Attachment -->
            <div class="attachment-block clearfix">
              <!-- 
        <div class="attachment-pushed">
          <h4 class="attachment-heading">
            <a href="http://www.lipsum.com/">Lorem ipsum text generator</a>
          </h4>


          <!-- /.attachment-text -->
              <!-- </div> -->
              <!-- /.attachment-pushed -->
            </div>
            <!-- /.attachment-block -->

            <!-- Social sharing buttons -->
            <a class="btn btn-default btn-xs" href="{{route('posts.like', $post->id)}}">
              <i class="fa fa-thumbs-o-up"></i> Like</a>
            <span class="pull-right text-muted">{{$post->likes}}|{{$post->comments->count()}}</span>
          </div>
          <!-- /.box-body -->
          <div class="box-footer box-comments">
            @foreach($comments as $c)
            <div class="box-comment">
              <!-- User image -->
              <img class="image img-sm" src="{{route('file', $c->user->detail->photo)}}" alt="User Image">

              <div class="comment-text">
                <span class="username">
                  {{$c->user->name}}
                  <span class="text-muted pull-right">{{$c->showDate()}}</span>
                </span>
                <!-- /.username -->
                {{$c->body}}
              </div>
              <!-- /.comment-text -->
            </div>
            @endforeach
          </div>
          <!-- /.box-footer -->
          <div class="box-footer">
            <form action="{{url('comentar')}}" method="POST">
              {{ csrf_field() }}
              <img class="img-responsive image img-sm" src="{{route('file', Auth::user()->detail->photo)}}" alt="Alt Text">
              <!-- .img-push is used to add margin to elements next to floating images -->
              <div class="img-push">
                <input type="text" name="body" class="form-control input-sm" placeholder="Press enter to post comment">
              </div>
              <input type="hidden" name="post_id" value="{{$post->id}}">
            </form>
          </div>
          <!-- /.box-footer -->
        </div>
      </div>
    </div>
  </div>
</div>
@endsection


