@extends('layouts.app') 
@section('content')

<div class="topmenu" style="padding-bottom: 20px;">
</div>
<div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6">
    <h2 class="page-header">Posts</h2>
        @if(session('message'))
        <div class="success alert-success">
            {{session('message')}}
        </div>
        @endif @if(session('error'))
        <div class="alert alert-danger">
            {{session('error')}}
        </div>
        @endif
    </div>
    <div class="col-md-3"></div>
</div>


@foreach($posts as $p)
<div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <div class="box box-solid">
            <div class="box-header">
                <a href="{{url('post/'.$p->id)}}">{{$p->title}}</a>
            </div>
            <div class="box-body">
                {{substr($p->body, 0, 140)}}(...)
            </div>
        </div>
    </div>
    <div class="col-md-3"></div>
</div>
@endforeach
<div class="paginate-button row">
    <div class="col-md-3"></div>
    <div class="col-md-6">
        {{$posts->links()}}
</div>
    </div>
    <div class="col-md-3"></div>
</div>
@endsection

@section('css')
<style>
    .paginate-button{
        width: 100%;
        height: 50px;
        margin: auto;
        bottom: 0;
        margin-bottom: 10px;
        position: absolute;
    }

</style>
@endsection