<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::group(['middleware' => ['auth']], function(){
  Route::get('/', 'PostController@index');
  Route::get('/post', 'PostController@create');
  Route::post('/post', 'PostController@store');
  Route::get('/post/{id}', 'PostController@show');
  Route::post('/atualizar/{id}', 'PostController@update');  
  Route::get('/editpost/{id}', 'PostController@edit');
  Route::get('/editdetail/{id}', 'UserDetailController@edit');
  Route::post('/atualizardetail/{id}', 'UserDetailController@update');
  //Route::get('/comentar/{id}', 'CommentController@create');
  Route::post('/comentar', 'CommentController@store');
  Route::get('/profile/create', 'UserDetailController@create')->name('criarperfil');
  Route::get('/profile/{id}', 'UserDetailController@index')->name('users.index');
  Route::post('/criaprofile/{id}', 'UserDetailController@store')->name('default.details');
  Route::get('/infopessoais/{id}', 'UserController@edit')->name('registrar.edit');
  Route::post('/infopessoais/{id}', 'UserController@update')->name('registrar.update');



  Route::get('/{folder?}/{filename?}', ['as' => 'file', 'uses' => function($folder, $filename) {
    return response()->file( storage_path() . '/app/' . $folder . '/' . $filename);
  }]);
Route::get('/post/{id}/count', 'PostController@count_like')->name('posts.like');
});
 



